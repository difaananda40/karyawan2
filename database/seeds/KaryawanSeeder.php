<?php

use Illuminate\Database\Seeder;
use App\Models\Karyawan;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $karyawan = Karyawan::create([
            'nip' => '001',
            'nama' => 'Ari',
            'jenis_kelamin' => 'male',
            'tanggal_lahir' => '1990-10-10',
            'tanggal_masuk' => '2018-10-10',
            'grade_id' => 1,
            'created_at' => Date('Y-m-d')
        ]);
    }
}
