<?php

use Illuminate\Database\Seeder;
use App\Models\Grade;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grade = Grade::insert([
            [
                'grade' => 'A',
                'gaji' => 1000000,
                'created_at' => Date('Y-m-d')
            ],
            [
                'grade' => 'B',
                'gaji' => 2000000,
                'created_at' => Date('Y-m-d')
            ],
            [
                'grade' => 'C',
                'gaji' => 3000000,
                'created_at' => Date('Y-m-d')
            ],
            [
                'grade' => 'D',
                'gaji' => 4000000,
                'created_at' => Date('Y-m-d')
            ]
        ]);
    }
}
