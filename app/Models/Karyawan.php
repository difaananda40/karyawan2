<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $fillable = [
        'nip',
        'nama',
        'is_male',
        'is_female',
        'tanggal_lahir',
        'tanggal_masuk',
        'grade_id',
        'jenis_kelamin'
    ];

    protected $appends = [
        'jenis_kelamin'
    ];

    public function grade() {
        return $this->belongsTo(Grade::class);
    }

    public function getJenisKelaminAttribute() {
        if($this->is_male) {
            return 'M';
        }
        else if($this->is_female) {
            return 'F';
        }
    }

    public function setJenisKelaminAttribute($value) {
        if($value === 'male') {
            $this->attributes['is_male'] = true;
            $this->attributes['is_female'] = false;
        }
        else if($value === 'female') {
            $this->attributes['is_male'] = false;
            $this->attributes['is_female'] = true;
        }
    }

    public function scopeFilter($query, $request) {
        if($request->has('search')) {
            $query->where('nama', 'like', '%' . $request->search . '%')
                  ->orWhere('nip', $request->search);
        }

        if($request->has('dari_tanggal') && $request->has('sampai_tanggal')) {
            $query->whereDate('tanggal_masuk', '>=', $request->dari_tanggal)
                  ->whereDate('tanggal_masuk', '<=', $request->sampai_tanggal);
        }
        return $query;
    }
}
