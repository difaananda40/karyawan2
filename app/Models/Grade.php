<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grade';
    protected $fillable = [
        'grade',
        'gaji'
    ];
    
    public function karyawan() {
        return $this->hasMany(Karyawan::class);
    }

    public function getGajiAttribute($value) {
        return number_format($value, 0);
    }
}
