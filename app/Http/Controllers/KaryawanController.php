<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Grade;

class KaryawanController extends Controller
{
    public function index(Request $request) {
        $karyawan = Karyawan::with('grade:id,grade,gaji')->filter($request)->get([
            'id',
            'nip',
            'nama',
            'is_male',
            'is_female',
            'tanggal_lahir',
            'tanggal_masuk',
            'grade_id',
        ]);
        return view('karyawan.index', ['karyawan' => $karyawan]);
    }

    public function create() {
        $grade = Grade::get(['id', 'grade', 'gaji']);
        return view('karyawan.create', ['grade' => $grade]);
    }

    public function store(Request $request) {
        $request->validate([
            'nip' => 'required|numeric|unique:karyawan',
            'nama' => 'required|max:255',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|date',
            'grade_id' => 'required|numeric'
        ]);
        $request->merge(['tanggal_masuk' => date('Y-m-d')]);
        $karyawan = Karyawan::create($request->all());
        return redirect()->route('karyawan.create')->with(['message' => 'Karyawan berhasil ditambah']);
    }

    public function edit($id) {
        $karyawan = Karyawan::find($id);
        $grade = Grade::get(['id', 'grade', 'gaji']);
        return view('karyawan.edit', ['karyawan' => $karyawan, 'grade' => $grade]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'nip' => 'required|numeric|unique:karyawan,nip,'. $id,
            'nama' => 'required|max:255',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|date',
            'grade_id' => 'required|numeric'
        ]);

        $karyawan = Karyawan::find($id);
        $karyawan->update($request->all());
        return redirect()->route('karyawan.index')->with(['message' => 'Karyawan berhasil diupdate']);
    }

    public function destroy($id) {
        $karyawan = Karyawan::find($id);
        $karyawan->delete();
        return redirect()->route('karyawan.index')->with(['message' => 'Karyawan berhasil di hapus']);
    }
}
