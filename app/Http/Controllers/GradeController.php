<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grade;

class GradeController extends Controller
{
    public function index() {
        $grade = Grade::get([
            'id',
            'grade',
            'gaji'
        ]);
        return view('grade.index', ['grade' => $grade]);
    }

    public function create() {
        return view('grade.create');
    }

    public function store(Request $request) {
        $request->validate([
            'grade' => 'required|max:1|unique:grade',
            'gaji' => 'required|numeric'
        ]);
        $grade = Grade::create($request->all());
        return redirect()->route('grade.create')->with(['message' => 'Grade berhasil ditambah']);
    }

    public function edit($id) {
        $grade = Grade::find($id);
        return view('grade.edit', ['grade' => $grade]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'grade' => 'required|max:1|unique:grade,grade,' . $id,
            'gaji' => 'required|numeric'
        ]);
        $grade = Grade::find($id);
        $grade->update($request->all());
        return redirect()->route('grade.index')->with(['message' => 'Grade berhasil diupdate']);
    }

    public function destroy($id) {
        $grade = Grade::with('karyawan')->findOrFail($id);
        if($grade->karyawan->count() > 0) {
            return redirect()->route('grade.index')->with(['message' => 'Grade berelasi tidak dapat dihapus']);
        }
        else {
            $grade->delete();
            return redirect()->route('grade.index')->with(['message' => 'Grade berhasil dihapus']);
        }
    }
}
