@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <h2>Tambah Grade</h2>
      </div>
    </div>
    @if ($msg = Session::get('message'))
      <div class="alert alert-info alert-block">
        {{ $msg }}
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                  Tambah Grade
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('grade.store') }}">
                      @csrf
                        <div class="form-group">
                          <label for="grade">Grade</label>
                          <input name="grade" type="text" class="form-control @error('grade') is-invalid @enderror" value="{{ old('grade') }}" id="grade" aria-describedby="grade" placeholder="Masukkan grade">
                          @error('grade')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div class="form-group">
                            <label for="gaji">Gaji</label>
                            <input name="gaji" type="number" class="form-control @error('gaji') is-invalid @enderror" value="{{ old('gaji') }}" id="gaji" aria-describedby="gaji" placeholder="Masukkan gaji">
                            @error('gaji')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group text-right">
                          <a class="btn btn-danger" href="{{ route('grade.index') }}">Cancel</a>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>
</div>

<script>
    $('.tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
</script>
@endsection
