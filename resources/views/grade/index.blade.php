@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <h2>List Grade</h2>
      </div>
      <div class="col-auto">
        <a class="btn btn-primary" href="{{ route('grade.create') }}">Tambah Grade +</a>
      </div>
    </div>
    @if ($msg = Session::get('message'))
      <div class="alert alert-info alert-block">
        {{ $msg }}
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col">
            <div class="table-responsive">
                <table class="table table-striped" id="tbl">
                  <thead class="thead-dark">
                    <tr class="text-center">
                      <th scope="col">Grade</th>
                      <th scope="col">Gaji</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($grade as $gr)
                      <tr class="text-center">
                        <th scope="row">{{ $gr->grade }}</th>
                        <td>{{ $gr->gaji }}</td>
                        <td class="d-flex justify-content-center">
                          <a href="{{ route('grade.edit', $gr->id) }}" class="btn btn-primary btn-sm">Edit</a>
                          <form method="POST" action="{{ route('grade.destroy', $gr->id) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm mx-1">Delete</button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
        </div>
    </div>
</div>
@endsection
