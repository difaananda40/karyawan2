@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <h2>Edit Karyawan</h2>
      </div>
    </div>
    @if ($msg = Session::get('message'))
      <div class="alert alert-info alert-block">
        {{ $msg }}
      </div>
    @endif
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                  Edit Karyawan
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('karyawan.update', $karyawan->id) }}">
                      @method('PATCH')
                      @csrf
                        <div class="form-group">
                          <label for="nip">NIP</label>
                          <input name="nip" type="number" class="form-control @error('nip') is-invalid @enderror" value="{{ $karyawan->nip }}" id="nip" aria-describedby="nip" placeholder="Masukkan NIP">
                          @error('nip')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input name="nama" type="text" class="form-control @error('nama') is-invalid @enderror" value="{{ $karyawan->nama }}" id="nama" aria-describedby="nama" placeholder="Masukkan nama">
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin</label>
                            <select class="form-control @error('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin" name="jenis_kelamin">
                              <option disabled selected hidden>Pilih jenis kelamin</option>
                              <option value="male" {{ ($karyawan->jenis_kelamin) === "M" ? 'selected' : '' }}>Laki-laki</option>
                              <option value="female" {{ ($karyawan->jenis_kelamin === "F") ? 'selected' : '' }}>Perempuan</option>
                            </select>
                            @error('jenis_kelamin')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                            <input name="tanggal_lahir" type="text" class="form-control tgl @error('tanggal_lahir') is-invalid @enderror" value="{{ $karyawan->tanggal_lahir }}" id="tanggal_lahir" aria-describedby="tanggal lahir" placeholder="Masukkan tanggal_lahir">
                            @error('tanggal_lahir')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="grade">Grade</label>
                            <select class="form-control @error('grade_id') is-invalid @enderror" id="grade" name="grade_id">
                              <option disabled selected hidden>Pilih grade</option>
                              @foreach ($grade as $gr)
                                  <option 
                                    value="{{ $gr->id }}"
                                    {{ ($karyawan->grade_id == $gr->id) ? 'selected' : '' }}
                                  >
                                    {{ $gr->grade }} - {{ $gr->gaji }}
                                  </option>
                              @endforeach
                            </select>
                            @error('grade_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group text-right">
                            <a class="btn btn-danger" href="{{ route('karyawan.index') }}">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                    </form>
                </div>
              </div>
        </div>
    </div>
</div>

<script>
    $('.tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
</script>
@endsection
