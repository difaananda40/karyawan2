@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-2">
      <div class="col">
        <h2>List Karyawan</h2>
      </div>
      <div class="col-auto">
        <a class="btn btn-primary" href="{{ route('karyawan.create') }}">Tambah Karyawan +</a>
      </div>
    </div>
    @if ($msg = Session::get('message'))
      <div class="alert alert-info alert-block mb-2">
        {{ $msg }}
      </div>
    @endif
    <div class="row justify-content-center">
      <div class="col-auto">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#filterTglModal">
          Filter
        </button>
      </div>
      <div class="col">
          <form method="POST" action="{{ route('karyawan.index') }}" role="search">
            @csrf
            @method('GET')
            <div class="input-group mb-3">
              <input name="search" type="text" class="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="button-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="submit" id="button-addon2">Search</button>
              </div>
          </div>
        </form>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <div class="table-responsive">
                <table class="table table-striped" id="tbl">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">NIP</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Gender</th>
                      <th scope="col">Tgl Lahir</th>
                      <th scope="col">Tgl Masuk</th>
                      <th scope="col">Grade</th>
                      <th scope="col">Gaji</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($karyawan as $kr)
                      <tr>
                        <th scope="row">{{ $kr->nip }}</th>
                        <td>{{ $kr->nama }}</td>
                        <td>{{ $kr->jenis_kelamin }}</td>
                        <td>{{ $kr->tanggal_lahir }}</td>
                        <td>{{ $kr->tanggal_masuk }}</td>
                        <td>{{ $kr->grade->grade }}</td>
                        <td>{{ $kr->grade->gaji }}</td>
                        <td class="d-flex">
                          <a href="{{ route('karyawan.edit', $kr->id) }}" class="btn btn-primary btn-sm mx-1">Edit</a>
                          <form method="POST" action="{{ route('karyawan.destroy', $kr->id) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm mx-1">Delete</button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="filterTglModal" tabindex="-1" role="dialog" aria-labelledby="filterTglModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="filterTglModalLabel">Filter</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="filterTgl" method="POST" action="{{ route('karyawan.index') }}">
              @csrf
              @method('GET')
                <div class="form-group">
                  <label for="dari_tanggal">Dari Tanggal</label>
                  <input name="dari_tanggal" type="text" class="form-control tgl" id="dari_tanggal" placeholder="Dari Tanggal">
                </div>
                <div class="form-group">
                  <label for="sampai_tanggal">Sampai Tanggal</label>
                  <input name="sampai_tanggal" type="text" class="form-control tgl" id="sampai_tanggal" placeholder="Sampai Tanggal">
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" form="filterTgl">Filter</button>
        </div>
      </div>
    </div>
  </div>

<script>
    $('.tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
</script>
@endsection
